import * as ChartBar from "./echarts/chart-bar";
import * as ChartGauge from "./echarts/chart-gauge";
import * as ChartGraph from "./echarts/chart-graph";
import * as ChartMix from "./echarts/chart-mix";
import * as ChartLine from "./echarts/chart-line";
import * as ChartPie from "./echarts/chart-pie";
import ExcelToJson from "./excelToJson"

export { default as Map } from "./map";

export {
    ChartBar,
    ChartGauge,
    ChartGraph,
    ChartMix,
    ChartLine,
    ChartPie,
    ExcelToJson
}
