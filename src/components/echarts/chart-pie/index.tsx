export interface SeriesItem {name: string; value: number}

export { default as Doughnut } from "./doughnut";
export { default as Nested } from "./nested";
export { default as Legend } from "./legend";
export { default as LegendCustom } from "./legend/legend";